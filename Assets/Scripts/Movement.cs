﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed;
    private float vertical;
    private float horizontal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey("w")&& !Input.GetKey("s")){
            transform.position += transform.TransformDirection(Vector3.forward * speed * Time.deltaTime);
        }
        else if(Input.GetKey("s")&& !Input.GetKey("w")){
            transform.position -= transform.TransformDirection(Vector3.forward * speed * Time.deltaTime);
        }
        else if(Input.GetKey("a")&& !Input.GetKey("d")){
            transform.position += transform.TransformDirection(Vector3.left * speed * Time.deltaTime);
        }
        else if(Input.GetKey("d")&& !Input.GetKey("a")){
            transform.position += transform.TransformDirection(Vector3.right * speed * Time.deltaTime);
        }
        horizontal += speed * Input.GetAxis("Mouse X");
        vertical -= speed * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(vertical,horizontal,0.0f);
    }
}
